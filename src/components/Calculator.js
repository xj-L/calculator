import React, {Component} from 'react';
import './../styles/calculator.css';
import Operator from './Operator';
import Input from './Input';

export default class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      output: '',
      number_1: '',
      number_2: '',
      operator: ''
    };
  }

  handleOperate = (operatorType) => {
    this.setState({
      operator: operatorType
    })
  }

  handleInputNum1 = (value) => {
    this.setState({
      number_1: value
    })
  }

  handleInputNum2 = (value) => {
    this.setState({
      number_2: value
    })
  }

  handleAC = () => {
    this.setState({
      result: '',
      number_1: '',
      number_2: '',
      operator: ''
    });
  }

  handleCalc = () => {
    let result= 0;
    const {number_1, number_2} = this.state;
    switch (this.state.operator) {
      case '+': result = +number_1 + +number_2;break;
      case '-': result = number_1 - number_2;break;
      case '/': result = number_1 / number_2;break;
      case '*': result = number_1 * number_2;break;
      default: return;
    }
    this.setState({
      result: result
    });
  }

  render() {
    return <div className="calculator">
      <input className="output" disabled value={this.state.result} type="text"/>
      <div className="operate-box">
        <div className="operator">
          <Operator handleOperate={this.handleOperate} operator="+"></Operator>
          <Operator handleOperate={this.handleOperate} operator="*"></Operator>
          <Operator handleOperate={this.handleOperate} operator="-"></Operator>
          <Operator handleOperate={this.handleOperate} operator="/"></Operator>
        </div>
        <div className="numbers">
          <Input handleInput={this.handleInputNum1} value={this.state.number_1}/>
          <Input handleInput={this.handleInputNum2} value={this.state.number_2}/>
        </div>
      </div>
      <div>
        <div className="AC-button" onClick={this.handleAC}>AC</div>
        <div className="result-button" onClick={this.handleCalc}>=</div>
      </div>
    </div>
  }
}