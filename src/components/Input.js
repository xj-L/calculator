import React, { Component } from 'react';

export default class Input extends Component{
  handleInput = (e) => {
    this.props.handleInput(e.target.value);
  }

  render() {
    return <input type="text" className="output" value={this.props.value} onChange={this.handleInput} />
  }
}