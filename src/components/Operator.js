import React, { Component } from 'react';

export default class Operator extends Component{
  handleClick = () => {
    this.props.handleOperate(this.props.operator);
  }

  render() {
    return <div className="operator-item" onClick={this.handleClick}>{this.props.operator}</div>
  }
}